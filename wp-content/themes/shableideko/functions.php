<?php


function registerScripts()
{
	wp_deregister_script( 'jquery' );

    wp_enqueue_script(
    	'jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js'
    );

	wp_enqueue_script(
		'custom', get_template_directory_uri() . '/js/custom.js', array('jquery'), uniqid(), true
	);
	wp_localize_script('custom', 'wpSharedData', [
		'ajaxRequestUrl' => admin_url('admin-ajax.php'),
		'security'       => wp_create_nonce('userSendContacts')
	]);

	wp_enqueue_script(
		'parallax', get_template_directory_uri() . '/js/libs/parallax.min.js', array('jquery'), '1.1', true
	);

    wp_enqueue_script(
        'jsCookies', get_template_directory_uri() . '/js/libs/jsCookies.js', '', '1.1', true
    );

	wp_enqueue_script(
		'sweetalert2', get_template_directory_uri() . '/js/libs/sweetalert2.min.js', array('jquery'), '1.1', true
	);

	wp_enqueue_script(
		'fontawesome', 'https://use.fontawesome.com/0b347342a5.js', '', uniqid(), true
	);

	wp_enqueue_script(
		'burgerMenu', get_template_directory_uri() . '/js/libs/burgerMenu.js', array('jquery'), '1.1', true
	);
}
add_action('wp_enqueue_scripts', 'registerScripts');



function registerStyles()
{
	wp_enqueue_style(
		'bootstrap', get_template_directory_uri() . '/css/libs/bootstrap.min.css'
	);

	wp_enqueue_style(
		'app', get_template_directory_uri() . '/css/app.css', '1.1', '1.1'
	);

	wp_enqueue_style(
		'burgerMenu', get_template_directory_uri() . '/css/burgerMenu.css', '', '1.1'
	);

	wp_enqueue_style(
		'media-styles', get_template_directory_uri() . '/css/media.css', '1.1', '1.1'
	);

	wp_enqueue_style(
		'sweetalert2', get_template_directory_uri() . '/css/libs/sweetalert2.min.css'
	);
}
add_action('wp_enqueue_scripts', 'registerStyles');


function customProductsPage()
{
	$labels = array(
		'name' => __( 'Products' ),
		'singular_name' => __( 'Product' )	
	);
	$supports = array(
		'title',
		'thumbnail',
		'editor',
		'page-attributes',
	);

	$args = array(
		'labels' => $labels,
		'public' => true,
		'has_archive' => true,
		'supports' => $supports,
		'hierarchical' => true,
		'exclude_from_search' => true
	);
	register_post_type('product', $args);
}
// Hooking up our function to theme setup
add_action( 'init', 'customProductsPage' );



function customAboutPage()
{
	$labels = array(
		'name' => __( 'About' ),
		'singular_name' => __( 'About' )	
	);
	$supports = array(
		'title',
		'editor',
	);

	$args = array(
		'labels' => $labels,
		'public' => true,
		'has_archive' => false,
		'supports' => $supports,
		'hierarchical' => false,
		'exclude_from_search' => true
	);
	register_post_type('about', $args);
}
// Hooking up our function to theme setup
add_action( 'init', 'customAboutPage' );




function themeSetup() {

	// Add featured image support
	add_theme_support('post-thumbnails');
	add_image_size('productThumbnail', 300, 300, true);

	//Navigation Menus
	register_nav_menus(array(
		'languageMenu' => __( 'Language Menu'),
		'footerLocations' => __('Locations Sub-Menu'),	
		'footerContacts' => __('Contacts Sub-Menu'),
		'footerHours' => __('Hours Sub-Menu'),
	));

	//Language support
	load_theme_textdomain('shableiDeko', get_template_directory() . '/languages');
}


add_action('after_setup_theme', 'themeSetup');




/* AJAX REQUESTS
 * 
 *
*/


add_action( 'wp_ajax_sendContactsByEmail', 'sendContactsByEmail' ); // For logged in users
add_action( 'wp_ajax_nopriv_sendContactsByEmail', 'sendContactsByEmail' ); // For anonymous users
function sendContactsByEmail()
{

	$validationArray = validate([
		'firstName' => 'required',
		'lastName'  => 'required',
		'email'     => 'required|email',
		'subject'   => 'required',
		'message'   => 'required'
	]);


	if(!empty($validationArray)) {

		wp_send_json_success($validationArray);

	} else {

		include_once 'mail/contactsMail.php';

		$firstName = $_POST['data']['firstName'];
		$lastName  = $_POST['data']['lastName'];
		$email     = $_POST['data']['email'];
		$subject   = $_POST['data']['subject'];
		$message   = $_POST['data']['message'];

		$to = 'support@shableideko.com';
		$body = getContactsEmailBody($firstName, $lastName, $email, $subject, $message);
		$headers[] = 'Content-Type: text/html; charset=UTF-8';
		$headers[] = 'From: ShableiDeko <shableideko@service.com>';


		wp_mail($to, 'New contact', $body, $headers);

		wp_send_json_success('success');

		wp_die();
	}
}

add_action( 'wp_ajax_sendSpecialOfferEmail', 'sendSpecialOfferEmail' ); // For logged in users
add_action( 'wp_ajax_nopriv_sendSpecialOfferEmail', 'sendSpecialOfferEmail' ); // For anonymous users
function sendSpecialOfferEmail()
{

    $validationArray = validate([
        'email'     => 'required|email',
    ]);


    if(!empty($validationArray)) {

        wp_send_json_success($validationArray);

    } else {

        include_once 'mail/specialOfferMail.php';

        $email     = $_POST['data']['email'];

        $to = 'support@shableideko.com';
        $body = getSpecialOfferEmailBody($email);
        $headers[] = 'Content-Type: text/html; charset=UTF-8';
        $headers[] = 'From: ShableiDeko <shableideko@service.com>';

        wp_mail($to, 'New special offer contact', $body, $headers);

        wp_send_json_success('success');

        wp_die();
    }
}

/**
 * Validates given data by given list of params
 * Available validation params:
 *  required - field is required
 *  email    - field should be a valid email
 *
 * @param $data
 * @return array
 */
function validate($data)
{
	$validationArray = [];

	foreach($data as $fieldName => $rules) {

		$rulesArray = explode('|', $rules);

		foreach($rulesArray as $rule) {
			if($rule === 'required') {

				if(empty($_POST['data'][$fieldName])) {
					array_push($validationArray, [
						'status'    => 'error',
						'field'     => $fieldName,
						'errorText' => getErrorText($fieldName, 'aa')
					]);
				}
			}

			if($rule === 'email') {

				if(!empty($_POST['data'][$fieldName])
					&& !filter_var($_POST['data'][$fieldName], FILTER_VALIDATE_EMAIL))
				{
					array_push($validationArray, [
						'status'    => 'error',
						'field'     => $fieldName,
						'errorText' => getErrorText($fieldName, 'email')
					]);
				}
			}
		}
	}

	return $validationArray;
}

function getErrorText($fieldName, $type)
{
	$fieldName = ucfirst($fieldName);

	if($type !== 'email') {
		return __("$fieldName field is required", "shableiDeko");
	}
	
	return __("$fieldName is not valid", "shableiDeko");
}

function mailtrap($phpmailer) {
    $phpmailer->isSMTP();
    $phpmailer->Host = 'smtp.mailtrap.io';
    $phpmailer->SMTPAuth = true;
    $phpmailer->Port = 2525;
    $phpmailer->Username = '2fe23929452923';
    $phpmailer->Password = 'ccb55d9a7b33d7';
}

add_action('phpmailer_init', 'mailtrap');


