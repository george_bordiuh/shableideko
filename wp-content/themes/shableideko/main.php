<?php

get_header();
?>


<header class="main-header"
        data-parallax="scroll"
        data-image-src="<?=bloginfo('template_url'); ?>/img/about.png" id="about"
>


    <div class="burger-click-region">
        <span class="burger-menu-piece"></span>
        <span class="burger-menu-piece"></span>
        <span class="burger-menu-piece"></span>
    </div>


    <div class="burger-menu">
        <ul class="burger-language">
            <?php pll_the_languages(); ?>
        </ul>
    </div>


    <div class="container">
        <div class="row">
            <div class="col-xs-2">
                <img src="<?=bloginfo('template_url'); ?>/img/whitepigeonpng.png" alt="" class="image-logo">
            </div>

            <div class="col-xs-10">



                <nav class="main-menu">
                    <ul class="inline-list">
                        <?php pll_the_languages();?>
                    </ul>
                </nav>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="company-name">
                    <div>
                        <?=_e("Shablei Deko", "shableiDeko");?>
                    </div>
                    <div class="company-info">
                        <?=_e("Handcrafted ceramic decorations for your home & garden", "shableiDeko");?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

<div class="content-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="content-block clearfix">
                    <div class="content-about">
                        <?php
                        $args = [
                            'post_type' => 'about',
                            'posts_per_page' => 1,
                        ];
                        $query = new WP_Query($args);

                        if($query->have_posts()): $query->the_post();
                            ?>

                            <h1 class="center-block"><?php the_title(); ?></h1>
                            <?php the_content(); ?>

                        <?php endif;?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="content-delimiter" data-parallax="scroll" data-image-src="<?php echo bloginfo('template_url'); ?>/img/ourproducts_cprs.jpg" id="ourProducts">
	<span class="content-delimiter-heading">
		<?=_e("Our products", "shableiDeko");?>
	</span>
</div>

<div class="content-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="image-group">

                    <?php
                    $args = array('post_type' => 'product');
                    $query = new WP_Query($args);

                    if($query->have_posts()):
                        while($query->have_posts()): $query->the_post();
                            ?>

                            <div class="col-md-4 col-sm-6 col-xs-6 col-xs-product">
                                <div class="content-block content-product">
                                    <?php the_post_thumbnail('productThumbnail'); ?>

                                    <h3>
                                        <?php the_title(); ?>
                                    </h3>

                                    <p>
                                        <?php the_content(); ?>
                                    </p>
                                </div>
                            </div>

                            <?php
                        endwhile;
                    endif;
                    ?>
                </div>
            </div>
        </div>
        <div class="catalog-info center-block">
            <p>
                <?=_e("If you are interested, and want to see more, you can get our full catalog", "shableiDeko");?>
            </p>
            <a class="btn btn-catalog" href="<?php echo bloginfo('template_url'); ?>/etc/ShableiDekoCatalogue.pdf" target="_blank">
                <?=_e("Here", "shableiDeko");?>
            </a>
        </div>
    </div>
</div>


<div class="content-delimiter" data-parallax="scroll" data-image-src="<?php echo bloginfo('template_url'); ?>/img/contact_cprs.jpg" id="contact">
    <span class="content-delimiter-heading">
        <?=_e("Contact", "shableiDeko");?>
    </span>
</div>

<div class="content-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <div class="contact-info">
                    <h1>
                        <?=_e("Contact us", "shableiDeko");?>
                    </h1>

                    <p>
                        <?=_e("If you have additional questions, or interested in partnership, you can contact with us per phone, or using the form below.", "shableiDeko");?>
                    </p>
                </div>

                <div class="content-block content-contact">
                    <form class="contact-form" id="contact-form">

                        <?php wp_nonce_field(basename(__FILE__), 'userSendContacts') ?>

                        <fieldset>
                            <legend>
                                <?=_e("Please complete the form below", "shableiDeko");?>
                            </legend>

                            <div class="form-group">
                                <label class="label-name">
                                    <?=_e("Name", "shableiDeko");?>
                                </label>

                                <div class="form-group-divider input-element input-firstName">
                                    <input type="text" name="firstName" id="firstName">
                                    <label for="firstName" data-text="<?=_e("First Name", "shableiDeko");?>">
                                        <?=_e("First Name", "shableiDeko");?>
                                    </label>
                                </div>

                                <div class="form-group-divider input-element">
                                    <input type="text" name="lastName" id="lastName">
                                    <label for="lastName" data-text="<?=_e("Last Name", "shableiDeko");?>">
                                        <?=_e("Last Name", "shableiDeko");?>
                                    </label>
                                </div>
                            </div>

                            <div class="form-group input-element">
                                <label for="email" data-text="<?=_e("Email Adreess*", "shableiDeko");?>">
                                    <?=_e("Email Adreess*", "shableiDeko");?>
                                </label>
                                <input type="text" name="email" id="email">
                            </div>

                            <div class="form-group input-element">
                                <label for="subject" data-text="<?=_e("Subject*", "shableiDeko");?>">
                                    <?=_e("Subject*", "shableiDeko");?>
                                </label>
                                <input type="text" name="subject" id="subject">
                            </div>

                            <div class="form-group input-element">
                                <label for="message" data-text="<?=_e("Message*", "shableiDeko");?>">
                                    <?=_e("Message*", "shableiDeko");?>
                                </label>
                                <textarea rows="3" name="message" id="message"></textarea>
                            </div>

                            <button type="submit" class="btn btn-submit" id="userSubmitButton">
                                <?=_e("SUBMIT", "shableiDeko");?>
                            </button>

                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<footer class="main-footer">
    <div class="container">
        <div class="main-footer-wrapper">

            <div class="back-to-top center-block">
                <i class="fa fa-angle-up" aria-hidden="true"></i>
                <a href="">
                    <?=_e("Top", "shableiDeko");?>
                </a>
            </div>

            <div class="row">
                <div class="col-md-6 col-sm-6">

                    <h3>
                        <?=_e("Contacts", "shableiDeko");?>
                    </h3>

                    <ul class="vertical-list">
                        <li>Email: shableideko@gmail.com</li>
                        <li><?=_e("Phone", "shableiDeko");?>: +48 791 521 084</li>
                        <li>+48 791 521 073</li>
                    </ul>

                </div>

                <div class="col-md-6 col-sm-6">
                    <h3>
                        <?=_e("Hours", "shableiDeko");?>
                    </h3>

                    <ul class="vertical-list">
                        <li>
                            <?=_e("Monday — Thursday", "shableiDeko");?>
                        </li>
                        <li>
                            <?=_e("8am — 11pm", "shableiDeko");?>
                        </li>
                        <li>
                            <?=_e("Friday — Sunday", "shableiDeko");?>
                        </li>
                        <li>
                            <?=_e("11am — 11pm", "shableiDeko");?>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>

<ul class="fixed-menu">
    <li class="jumper" data-href="#about">
        <a>
            <?=_e("About", "shableiDeko");?>
        </a>
    </li>
    <li class="jumper" data-href="#ourProducts">
        <a>
            <?=_e("Our Products", "shableiDeko");?>
        </a>
    </li>
    <li class="jumper" data-href="#contact">
        <a>
            <?=_e("Contact", "shableiDeko");?>
        </a>
    </li>
</ul>

<a class="btn-catalog-fixed" href="<?php echo bloginfo('template_url'); ?>/etc/ShableiDekoCatalogue.pdf" target="_blank">
    <?=_e("Check out our free catalog!", "shableiDeko");?>
</a>

<?php get_footer(); ?>


