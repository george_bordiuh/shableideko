function showSweetAlertSuccessMessage()
{
	swal({
		title: 'We will contact you as soon as we can',
		type: 'success',
		showCloseButton: true
	}).then(
		function () {
            var languageCookie = Cookies.get('pll_language');
            window.location.replace('/' + languageCookie);
		}
	);
}

function showFormErrorMessages(data)
{
	data.forEach(function (item) {
		if (item.field) {
			$('label[for=' + item.field + ']')
				.text(item.errorText)
				.addClass('form-warning')
		}
	});
}

function showSpecialOffersPopUp() {
    swal({
        title: app.specialOffersHeading,
        input: 'email',
		text: app.specialOfferText,
        showCancelButton: true
    }).then(function (email) {
        var formData = {
            'email'     : email
		};

        ajaxRequest(formData, 'sendSpecialOfferEmail');
    })
}

//Handling user's contacts request
var ajaxRequest = function(formData, action) {
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: wpSharedData.ajaxRequestUrl,
        data: {
            action: action,
            data: formData,
            security: wpSharedData.security
        },
        success: function(res) {
            var data = res.data;

            if(data === 'success') {
                showSweetAlertSuccessMessage();
            } else {
                showFormErrorMessages(data);
            }
        }
    });
};

$(document).ready(function () {

	//In seconds
	var specialOfferPopUpTimer = 120;

	//Show special offer popUp
    window.setInterval(function () {
		showSpecialOffersPopUp();
    }, specialOfferPopUpTimer * 1000);

	//Burger menu
	$('.burger-click-region').click(function() {

        var body = $('body');
        var burgerMenu = $('.burger-menu');
        
        var clickDelay      = 500,
            clickDelayTimer = null;

        if(clickDelayTimer === null) {

            var $burger = $(this);
            $burger.toggleClass('active');
            $burger.parent().toggleClass('is-open');

            body.animate({
                right: '75%'
            }, 200).addClass('turn-off-scroll');

            burgerMenu.animate({
                right: '0px'
            }, 200);

            if(!$burger.hasClass('active')) {
                $burger.addClass('closing');
                body.animate({
                    right: '0'
                }, 200).removeClass('turn-off-scroll');

                burgerMenu.animate({
                    right: '-999999px'
                }, 200);
            }

            clickDelayTimer = setTimeout(function () {
                $burger.removeClass('closing');
                clearTimeout(clickDelayTimer);
                clickDelayTimer = null;
            }, clickDelay);
        }
	});


	//Jumper for fixed side menu
	$('.jumper').on('click', function( e ) {

	    e.preventDefault();

	    $('body, html').animate({
	        scrollTop: $( $(this).data('href') ).offset().top
	    }, 600);

	});

	//Back to the start of the page button
	$('.back-to-top').click(function(){
		$('html, body').animate({scrollTop : 0},800);
		return false;
	});

	$('#userSubmitButton').on('click', function(event) {
		event.preventDefault();

		var formData = {
			'firstName' : $("#firstName").val(),
			'lastName'  : $("#lastName").val(),
			'email'     : $("#email").val(),
			'subject'   : $("#subject").val(),
			'message'   : $("#message").val()
		};

		ajaxRequest(formData, 'sendContactsByEmail');
	});

	//Clear form warnings
	$("#contact-form textarea, #contact-form input").on('change keyup paste', function(event) {
		var labels = $(this).parent('.input-element').find('label');
		labels.text(labels.data('text')).removeClass('form-warning');
	});


});
