<!DOCTYPE html>
<html lang="en">

<?php wp_head(); ?>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<meta charset="UTF-8">
	<link rel="icon" href="whitepigeonicon.ico">
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
	<title><?=get_bloginfo();?></title>

    <script>
        var app = {};

        app.specialOfferText = "<h1>" +
            "<?=_e("Learn our special offers and promotions now!", "shableiDeko");?>" +
            "</h1>" +
            "<p>" +
            "<?=_e('Call us +48791521084 or leave us your email and we will contact you!');?>" +
            "</p>";

        app.specialOffersHeading = "<?=_e("Special offer", "shableiDeko");?>";
        app.successMessageText = "<?=_e("SuccessMessageText", "shableiDeko");?>"
    </script>

</head>

<body>
	
