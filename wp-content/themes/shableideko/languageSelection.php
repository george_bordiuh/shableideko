<?php

get_header();
?>

	<div class="startingPage-wrapper">
		<div class="startingPage-logo">
            <img src="<?=bloginfo('template_url'); ?>/img/finalpigeonyeswhitesmall.ico" alt="">
		</div>

        <div class="startingPage-content">
            <h1 class="startingPage-mainHeading">Shablei deko</h1>
            <h4 class="startingPage-about">Handcrafted ceramic decorations for your home & garden.</h4>

            <ul class="inline-list">
                <?php pll_the_languages();?>
            </ul>
        </div>

	</div>

<?php get_footer(); ?>


